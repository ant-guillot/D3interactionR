HTMLWidgets.widget({

  name: 'D3interactionR',

  type: 'output',

  factory: function(el, width, height) {

    // TODO: define shared variables for this instance

    return {

      renderValue: function(x) {
var matrix = x.data_in;

var svg = d3.select(el).append("svg")
      .attr('class',"D3interactionRChart")
      .attr("width", 900)
      .attr("height", 900),
    width = 900,
    height = 900,
    outerRadius = Math.min(width, height) * 0.5 - 40,
    innerRadius = outerRadius - 30;

var formatValue = d3.formatPrefix(",.0", 1e3);

var chord = d3.chord()
    .padAngle(0.05)
    .sortSubgroups(d3.descending);

var arc = d3.arc()
    .innerRadius(innerRadius)
    .outerRadius(outerRadius);

var ribbon = d3.ribbon()
    .radius(innerRadius);

var color = d3.scaleOrdinal(d3.schemeCategory20);

var g = svg.append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
    .datum(chord(matrix));

var group = g.append("g")
    .attr("class", "groups")
    .selectAll("g")
    .data(function(chords) { return chords.groups; })
    .enter()
    .append("g")
    .attr("class", "group")
    ;

group.append("path")
    .style("fill", function(d) { return color(d.index); })
    .style("stroke", function(d) { return d3.rgb(color(d.index)).darker(); })
    .attr("d", arc);

var groupTick = group.selectAll(".group-tick")
    .data(function(d) { return groupTicks(d, 1e3); })
    .enter().append("g")
    .attr("class", "group-tick")
    .attr("transform", function(d) { return "rotate(" + (d.angle * 180 / Math.PI - 90) + ") translate(" + outerRadius + ",0)"; });

groupTick.append("line")
    .attr("x2", 6);

groupTick
  .filter(function(d) { return d.value % 5e3 === 0; })
  .append("text")
    .attr("x", 8)
    .attr("dy", ".35em")
    .attr("transform", function(d) { return d.angle > Math.PI ? "rotate(180) translate(-16)" : null; })
    .style("text-anchor", function(d) { return d.angle > Math.PI ? "end" : null; })
    .text(function(d) { return formatValue(d.value); });

g.append("g")
    .attr("class", "ribbons")
    .selectAll("path")
    .data(function(chords) { return chords; })
    .enter().append("path")
    .attr("class","ribbon")
    .attr("d", ribbon)
    .style("fill", function(d) { return color(d.target.index); })
    .style("stroke", function(d) { return d3.rgb(color(d.target.index)).darker(); })
//action when hoovering a ribbon
    .on("mousemove", function(d) {
      d3.selectAll(".ribbon")
        .style("opacity", 0.3);
      d3.select(this)
        .style("opacity",1);
      var selected_ribbon=d;
      d3.selectAll(".group")
        .style("opacity", 0.3);

      d3.selectAll(".group")
        .filter(function(d)
          {
            return (d.index==selected_ribbon.source.index
            | d.index==selected_ribbon.source.subindex )
          }
          )
        .style("opacity", 1);
    })
    .on("mouseout",function(){
      d3.selectAll(".ribbon")
        .style("opacity", 0.67);
    }
    );

//action when hoovering a group
d3.selectAll(".group")
  .on("mousemove", function(d) {
    var selected_group=d;
    d3.selectAll(".ribbon")
        .style("opacity", 0.3);
    d3.selectAll(".ribbon")
      .filter(function(d)
        {
          return (selected_group.index==d.source.index | selected_group.index==d.source.subindex )
        }
        )
      .style("opacity", 1);
    d3.selectAll(".group")
      .style("opacity", 0.3)
    d3.select(this)
        .style("opacity",1);

    })
    .on("mouseout", function(d) {
      d3.selectAll(".group").style("opacity", 0.8)
      d3.selectAll(".ribbon").style("opacity", 0.67)
    })



    // Returns an array of tick angles and values for a given group and step.
    function groupTicks(d, step) {
      var k = (d.endAngle - d.startAngle) / d.value;
      return d3.range(0, d.value, step).map(function(value) {
        return {value: value, angle: value * k + d.startAngle};
      });
    }




      },

      resize: function(width, height) {

        // TODO: code to re-render the widget with a new size

      }

    };
  }
});
